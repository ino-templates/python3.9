# Python 3.9

## Env

- Python = "^3.9"
- Pyenv  = "^1.2.23"
- Poetry = "^1.1.4"

## Packages



## VSCode Extensions

- Python  = "v2021.2.576481509"
- Pylance = "v2021.2.3"

## Setup

```
$ mkdir my-dir && cd my-dir
$ pyenv local 3.9.1          # check your pyenv, ex) `pyenv versions` or `pyenv install --list`
$ poetry install
$ poetry shell
$ python --version           # it will return `3.9.1`
```

